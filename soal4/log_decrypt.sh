#!/bin/bash

filename="$1"
currentHour=$(echo "${filename:0:2}" | sed 's/^0//')

inputFile="${filename}"
outputFile="decrypted_${filename}"

echo "Sedang mendekripsi"

while read line
do
    output=""
    for ((i=0; i<${#line}; i++)); do
        char=${line:$i:1}
        isUp=0
        if [[ "$char" =~ [a-zA-Z] ]]; then
            desChar=$(printf '%d' "'$char")
            if [[ $desChar -ge 65 && $desChar -le 90 ]]; then
                isUp=1
            fi
            if (( $desChar - currentHour < 65 && $isUp )) || (( $desChar - currentHour < 97 && ! $isUp )); then
                charDekrip=$(printf \\$(printf '%03o' "$(( $desChar - currentHour + 26 ))"))
                output="${output}${charDekrip}"
            else
                charDekrip=$(printf \\$(printf '%03o' "$(( $desChar - currentHour ))"))
                output="${output}${charDekrip}"
            fi
        else
            output="${output}${char}"
        fi
    done

    echo "${output}" >> "${outputFile}"
done < "${inputFile}"
echo "Selesai"
